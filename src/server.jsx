// todo hash from webpack 
// todo sub component match
// todo ...

import path from "path";
import express from "express";

import React from "react";
import { renderToString } from "react-dom/server";
import { matchPath } from 'react-router-dom'
import { StaticRouter } from 'react-router';

import template from "./index.html";
import { App, routes } from "./App";

var app = express();

app.use('/static', express.static(path.join(path.dirname(process.mainModule.filename), "static")));


app.get('*', function (req, res) {
    const context = {}

    const render = (data) => {
        const APP_HTML = renderToString(
            <StaticRouter location={req.url} context={context}>
                <App data={data} />
            </StaticRouter>
        )

        var content = template({
            APP_HTML: APP_HTML,
            APP_STATE: JSON.stringify(data)
        });
        
        res.status(context.status || 200).send(content);
    };

    const promises = []

    Promise.all(promises).then(all => all.map(r => r.data)).then(data => {
        render(data.length > 0 ? data : null);
    });

});

app.listen(9009, function () {
    console.log('Example app listening on port 9009!');
})