import * as React from 'react'
import axios from 'axios';

import Map from "./Map";

require('./styles/styles.less');


export class App extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = {
            state: "new",
            team1: 0,
            team2: 0,
        }
    }
    
    changeStateTo = (state) => {
        this.setState({...this.state, state:state});
    }
    
    changeToRunning = () => {
        this.setState({state:"running"});
    }
    
    changeToStop = () => {
        this.setState({state:"running"});
    }
    
    updateScore = (beacons) => {
        
        const team1 = beacons.features.filter(a => a.properties.owner === 1).length;
        const team2 = beacons.features.filter(a => a.properties.owner === 2).length;

        this.setState({
            ...this.state,
            team1: this.state.team1 + team1,
            team2: this.state.team2 + team2
        })

    }

    render() {
        return (
            <div className="App">
                <div className="Navigation">
                    
                    {this.state.state == "new" && 
                        <button onClick={() => this.changeStateTo("running")}>Start</button>
                    }
                    
                    {this.state.state == "running" && 
                        <button onClick={() => this.changeStateTo("stop")}>Stop</button>
                    }
                    
                    {this.state.state == "stop" && 
                        <button onClick={() => this.changeStateTo("new")}>Reset</button>
                    }

                </div>
                <div className="team team-1">
                    <div className="header">
                        TEAM 1
                    </div>
                    <div className="score">{this.state.team1}</div>
                </div>
                <Map state={this.state.state} updateScore={this.updateScore}/>
                <div className="team team-2">
                    <div className="header">
                        TEAM 2
                    </div>
                    <div className="score">{this.state.team2}</div>
                </div>
            </div>
        )
    }
}